# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

# Check module version
check_mofem_version(0 4 4)

if(NOT WITH_TETGEN)
  message(FATAL_ERROR "MoFEM have to be installed with TetGen")
endif(NOT WITH_TETGEN)

add_subdirectory(${PROJECT_SOURCE_DIR}/optimal_mass_transport)

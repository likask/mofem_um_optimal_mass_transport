/** \file OptimalMassTransport.hpp
* \ingroup optimal_mass_transport

* \brief Header file for Implementation of optimal mass transport
* See details in \cite barrett2007mixed

*/

/*
* This file is part of MoFEM.
* MoFEM is free software: you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the
* Free Software Foundation, either version 3 of the License, or (at your
* option) any later version.
*
* MoFEM is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
* License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __OPTIMALMASSTRANSPORT_HPP__
#define __OPTIMALMASSTRANSPORT_HPP__

struct OptimalMassTransport {

  MoFEM::Interface &mField;

  /// \brief  definition of volume element
  struct VolumeFE: public MoFEM::VolumeElementForcesAndSourcesCore {
    VolumeFE(MoFEM::Interface &m_field): MoFEM::VolumeElementForcesAndSourcesCore(m_field) {}

    /**
     * \brief set quadrature rank
     * @param  order order of element passed to funtion by MoFEM
     * @return       quadrature rule
     */
    int getRule(int order) { return 2*order+1; };
  };

  /** \brief define surface element
    *
    */
  struct TriFE: public MoFEM::FaceElementForcesAndSourcesCore {
    TriFE(MoFEM::Interface &m_field): MoFEM::FaceElementForcesAndSourcesCore(m_field) {}
    /**
     * \brief set quadrature rank
     * @param  order order of element passed to funtion by MoFEM
     * @return       quadrature rule
     */
    int getRule(int order) { return 2*order+1; };
  };

  OptimalMassTransport(MoFEM::Interface &m_field):
  mField(m_field) {}
  virtual ~OptimalMassTransport() {}

  /**
   * \brief data structure storing material constants, model parameters, matrices, etc.
   *
   */
  struct BlockData {

    // field
    const string fluxFieldName;
    const string concentrationFieldName;
    const string kFieldName;
    const string volFeName;
    const string sufFeName;

    double r;     //< regularisation parameter
    double eps;   //< have to be small but larger than zero

    int oRder; ///< approximation order

    // Petsc data
    DM dM;
    Mat A;
    Vec D,F;

    BlockData():
    fluxFieldName("Flux"),
    concentrationFieldName("U"),
    kFieldName("k"),
    volFeName("VolFE"),
    sufFeName("SurfFE"),
    r(2), //< go very easy for a start
    eps(0.1) //> go very easy for a start
    {
    }
    ~BlockData() {}

  };

  BlockData blockData;

  PetscErrorCode genrateMeshWithTetgen(const string file_name) {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  PetscErrorCode createFields() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  PetscErrorCode createFe() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  PetscErrorCode createProblem() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  PetscErrorCode solveProblem() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  PetscErrorCode postProcesResults() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  PetscErrorCode destroyProblem() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

  struct OpEtaTimesDivQ: public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

    BlockData &blockData;
    OpEtaTimesDivQ(BlockData &data):
    MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
      data.concentrationFieldName,data.fluxFieldName,UserDataOperator::OPROWCOL
    ),
    blockData(data) {
      sYmm = false;
    }

    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSurcesCore::EntData &row_data,
      DataForcesAndSurcesCore::EntData &col_data
    ) {
      PetscFunctionBegin;
      PetscFunctionReturn(0);
    }

  };

  struct OpEtaTimesF: public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

    BlockData &blockData;
    OpEtaTimesF(BlockData &data):
    MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
      data.concentrationFieldName,UserDataOperator::OPROW
    ),
    blockData(data) {
      sYmm = false;
    }

    PetscErrorCode doWork(
      int row_side,
      EntityType row_type,
      DataForcesAndSurcesCore::EntData &row_data
    ) {
      PetscFunctionBegin;
      PetscFunctionReturn(0);
    }

  };

  struct OpEtaTimesDivQ: public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

    BlockData &blockData;
    OpEtaTimesDivQ(BlockData &data):
    MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
      data.concentrationFieldName,data.fluxFieldName,UserDataOperator::OPROWCOL
    ),
    blockData(data) {
      sYmm = false;
    }

    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSurcesCore::EntData &row_data,
      DataForcesAndSurcesCore::EntData &col_data
    ) {
      PetscFunctionBegin;
      PetscFunctionReturn(0);
    }

  };

  struct OpDivVTimesU: public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

    BlockData &blockData;
    OpDivVTimesU(BlockData &data):
    MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
      data.fluxFieldName,data.concentrationFieldName,UserDataOperator::OPROWCOL
    ),
    blockData(data) {
      sYmm = false;
    }

    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSurcesCore::EntData &row_data,
      DataForcesAndSurcesCore::EntData &col_data
    ) {
      PetscFunctionBegin;
      PetscFunctionReturn(0);
    }

  };

  struct OpVTimesQ: public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

    BlockData &blockData;
    OpVTimesQ(BlockData &data):
    MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
      data.fluxFieldName,data.concentrationFieldName,UserDataOperator::OPROWCOL
    ),
    blockData(data) {
      sYmm = false;
    }

    PetscErrorCode doWork(
      int row_side,int col_side,
      EntityType row_type,EntityType col_type,
      DataForcesAndSurcesCore::EntData &row_data,
      DataForcesAndSurcesCore::EntData &col_data
    ) {
      PetscFunctionBegin;
      PetscFunctionReturn(0);
    }

  };





};

#endif

/***************************************************************************//**
* \defgroup optimal_mass_transport Optimal mass transport
* \ingroup user_modules
*
* \brief Implementation of optimal mass transport using mix element
*
******************************************************************************/
